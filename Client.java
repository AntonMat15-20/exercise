package ru.ru.socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Класс для создания сокета.
 */
public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("localhost", 8080);
             DataInputStream input = new DataInputStream(socket.getInputStream());
             DataOutputStream output = new DataOutputStream(socket.getOutputStream())) {

            while (true) {
                String response = InputString();
                output.writeUTF(response);
                output.flush();
                System.out.println("отправлено серверу: " + response);
                if (response.equals("client stop")) {
                    System.out.println("Конец связи");
                    break;
                }
                response = input.readUTF();
                System.out.println("прислал сервер: " + response);
                Thread.sleep(1);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static String InputString() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите слово ");
        return scanner.nextLine();
    }
}