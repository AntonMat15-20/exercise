package ru.ru.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Класс который создаёт client server.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждёт клиента....");

        try (Socket clientSocket = serverSocket.accept();
             DataInputStream input = new DataInputStream(clientSocket.getInputStream());
             DataOutputStream output = new DataOutputStream(clientSocket.getOutputStream())) {

            System.out.println("Новое соединение: " + clientSocket.getInetAddress().toString());
            String request;
            while ((request = input.readUTF()) != null) {
                System.out.println("прислал клиент: " + request);
                output.writeUTF(request);
                output.flush();
                if (request.equals("client stop")) {
                    break;
                }
                System.out.println("отправлено клиенту: " + request);
                Thread.sleep(1);
            }
            System.out.println("Клиент отключился");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}